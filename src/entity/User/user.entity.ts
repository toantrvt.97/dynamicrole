import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToMany } from 'typeorm';
import { RoleEntity } from '../role/role.entity';
import { GroupRoleEntity } from '../groupRole/group-role.entity';
import { GroupEntity } from '../group/group.entity';

@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    roleName: string;

    @Column()
    groupName: string;

    @Column()
    roleId: string;
    @ManyToOne( type => RoleEntity, role => role.users)
    role: RoleEntity;

    @OneToMany( type => GroupRoleEntity, grouprole => grouprole.user)
    groupRoleUsers: GroupRoleEntity[];

    @Column()
    groupId: string;
    @ManyToOne(type => GroupEntity, group => group.users)
    group: GroupEntity;

}
