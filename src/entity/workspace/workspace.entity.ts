import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToMany } from 'typeorm';
import { ApplicationEntity } from '../application/application.entity';
import { GroupEntity } from '../group/group.entity';

@Entity()
export class WorkspaceEntity {

    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    appId: string;

    @Column()
    appName: string;

    @ManyToOne(type => ApplicationEntity, app => app.workspaces)
    app: string;

    @OneToMany(type => GroupEntity, group => group.workspace)
    groups: GroupEntity[];

}
