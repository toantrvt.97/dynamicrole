import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToMany } from 'typeorm';
import { FunctionEntity } from '../function/function.entity';
import { UserEntity } from '../User/user.entity';
import { GroupRoleEntity } from '../groupRole/group-role.entity';

@Entity()
export class RoleEntity {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    rule: boolean;

    @Column()
    functionName: string;
    @Column()
    ftionId: string;
    @ManyToOne( type => FunctionEntity, ftion => ftion.roles)
    ftion: FunctionEntity;

    @OneToMany( type => UserEntity, user => user.role)
    users: UserEntity[];

    @OneToMany( type => GroupRoleEntity, groupRole => groupRole.role)
    groupRoles: GroupRoleEntity[];
}
