import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from 'typeorm';
import { RoleEntity } from '../role/role.entity';

@Entity()
export class FunctionEntity {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @OneToMany( type => RoleEntity, role => role.ftion)
    roles: RoleEntity[];
}
