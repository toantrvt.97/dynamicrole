import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from 'typeorm';
import { WorkspaceEntity } from '../workspace/workspace.entity';

@Entity()
export class ApplicationEntity {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @OneToMany(type => WorkspaceEntity, workspace => workspace.app)
    workspaces: WorkspaceEntity[];
}
