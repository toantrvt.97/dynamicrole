import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToMany } from 'typeorm';
import { WorkspaceEntity } from '../workspace/workspace.entity';
import { GroupRoleEntity } from '../groupRole/group-role.entity';
import { UserEntity } from '../User/user.entity';

@Entity()
export class GroupEntity {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @Column({ nullable: true })
    workspaceId: string ;

    @Column({default: null})
    workspaceName: string;

    @ManyToOne(type => WorkspaceEntity, workspace => workspace.groups)
    workspace: WorkspaceEntity;

    @OneToMany(type => GroupRoleEntity, groupRole => groupRole.group)
    roleGroups: GroupRoleEntity[];

    @OneToMany(type => UserEntity, user => user.group)
    users: UserEntity[];
}
