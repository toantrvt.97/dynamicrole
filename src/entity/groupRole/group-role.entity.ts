import { PrimaryGeneratedColumn, Entity, Column, ManyToOne, ViewEntity, Connection } from 'typeorm';
import { UserEntity } from '../User/user.entity';
import { RoleEntity } from '../role/role.entity';
import { GroupEntity } from '../group/group.entity';

@Entity()
export class GroupRoleEntity {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    rule: boolean;

    @Column()
    userId: string;
    @ManyToOne( type => UserEntity, user => user.groupRoleUsers)
    user: UserEntity;

    @Column()
    roleId: string;
    @ManyToOne( type => RoleEntity, role => role.groupRoles)
    role: RoleEntity;

    @Column()
    groupId: string;
    @ManyToOne( type => GroupEntity, group => group.roleGroups)
    group: GroupEntity;

    @Column({default: null})
    userName: string;

    @Column({default: null})
    roleName: string;

    @Column({default: null})
    groupName: string;
}
