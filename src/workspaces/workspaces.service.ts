import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { WorkspaceEntity } from 'src/entity/workspace/workspace.entity';
import { Repository, DeleteResult } from 'typeorm';
import { CreateWorkspaceDto } from 'src/dto/workspace/create-workspace.dto';
import { CreateUserDto } from 'src/dto/user/create-user.dto';
import { ApplicationEntity } from 'src/entity/application/application.entity';

@Injectable()
export class WorkspacesService {
    constructor(
    @InjectRepository(WorkspaceEntity)
    private readonly workspaceRepository: Repository<WorkspaceEntity>,
    @InjectRepository(ApplicationEntity)
    private readonly appRepository: Repository<ApplicationEntity>) { }

    async insert(workspaceDetails: CreateWorkspaceDto): Promise<WorkspaceEntity> {
        const {name, appId} = workspaceDetails;
        const workspaces = new WorkspaceEntity();
        workspaces.name = name;
        workspaces.appId = appId;
        return await this.workspaceRepository.save(workspaces);
    }
    async getAllWorkspace(): Promise<WorkspaceEntity[]> {
        return this.workspaceRepository.find();
    }
    async update( id: string, workspaceDetails: CreateWorkspaceDto): Promise<WorkspaceEntity> {
        const workspace = await this.workspaceRepository.findOne(id);
        const updated = Object.assign(workspace, workspaceDetails);
        return await this.workspaceRepository.save(updated);
    }
    async delete(id: string): Promise<DeleteResult> {
        return await this.workspaceRepository.delete(id);
    }
    async geWtWorkspaceDetail(): Promise<WorkspaceEntity[]> {
        const detail = await this
        .workspaceRepository
        .createQueryBuilder('workspace')
        .select('workspace.id')
        .addSelect('workspace.name')
        .addSelect('workspace.appName')
        .leftJoin(ApplicationEntity, 'app', 'app.id = workspace.appId')
        .getMany();
        return detail;
    }
}
