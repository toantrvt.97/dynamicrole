import { Module } from '@nestjs/common';
import { WorkspacesController } from './workspaces.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WorkspaceEntity } from 'src/entity/workspace/workspace.entity';
import { WorkspacesService } from './workspaces.service';
import { ApplicationEntity } from 'src/entity/application/application.entity';

@Module({
  imports: [TypeOrmModule.forFeature([WorkspaceEntity]), TypeOrmModule.forFeature([ApplicationEntity])],
  controllers: [WorkspacesController],
  providers: [WorkspacesService],
  exports: [WorkspacesService],
})
export class WorkspacesModule {}
