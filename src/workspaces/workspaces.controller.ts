import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { WorkspacesService } from './workspaces.service';
import { WorkspaceEntity } from 'src/entity/workspace/workspace.entity';
import { CreateWorkspaceDto } from 'src/dto/workspace/create-workspace.dto';

@Controller('api/workspaces')
export class WorkspacesController {
    constructor(private readonly workspacesService: WorkspacesService) {}

    @Get()
    async findAll(): Promise<WorkspaceEntity[]> {
        return this.workspacesService.geWtWorkspaceDetail();
    }

    @Post('post')
    async create(@Body() createWorkspace: CreateWorkspaceDto) {
        try {
            return await this.workspacesService.insert(createWorkspace);
        } catch (Error) {
            return Error;
        }
    }
    @Put('update/:id')
    async update(@Param() id: string, @Body() updateWorkspaceDto: CreateWorkspaceDto) {
        try {
            return await this.workspacesService.update(id, updateWorkspaceDto);
        } catch (Error)  {
            return Error;
        }
    }
    @Delete('detele/:id')
    async delete(@Param() id: string) {
        return await this.workspacesService.delete(id);
    }
}
