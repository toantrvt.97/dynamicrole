import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GroupsModule } from './groups/groups.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApplicationsController } from './applications/applications.controller';
import { ApplicationsModule } from './applications/applications.module';
import { FunctionsService } from './functions/functions.service';
import { FunctionsModule } from './functions/functions.module';
import { GroupRolesService } from './group-roles/group-roles.service';
import { RolesService } from './roles/roles.service';
import { UsersService } from './users/users.service';
import { WorkspacesService } from './workspaces/workspaces.service';
import { WorkspacesModule } from './workspaces/workspaces.module';
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { GroupRolesModule } from './group-roles/group-roles.module';

@Module({
  imports: [
      TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'sa',
      password: 'VietToan97',
      database: 'dynamicfunction',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }), GroupsModule, ApplicationsModule, FunctionsModule, WorkspacesModule, UsersModule, RolesModule, GroupRolesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
