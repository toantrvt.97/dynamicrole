import { Module } from '@nestjs/common';
import { GroupRolesController } from './group-roles.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupRoleEntity } from 'src/entity/groupRole/group-role.entity';
import { GroupRolesService } from './group-roles.service';
import { RoleEntity } from 'src/entity/role/role.entity';
import { UserEntity } from 'src/entity/User/user.entity';
import { GroupEntity } from 'src/entity/group/group.entity';

@Module({
  imports:
  [TypeOrmModule.forFeature([GroupRoleEntity]),
  TypeOrmModule.forFeature([RoleEntity]),
  TypeOrmModule.forFeature([UserEntity]),
  TypeOrmModule.forFeature([GroupEntity])],
  controllers: [GroupRolesController],
  providers: [GroupRolesService],
  exports: [GroupRolesService],

})
export class GroupRolesModule {}
