import { Test, TestingModule } from '@nestjs/testing';
import { GroupRolesController } from './group-roles.controller';

describe('GroupRoles Controller', () => {
  let controller: GroupRolesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GroupRolesController],
    }).compile();

    controller = module.get<GroupRolesController>(GroupRolesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
