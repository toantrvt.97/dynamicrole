import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { GroupRolesService } from './group-roles.service';
import { GroupRoleEntity } from 'src/entity/groupRole/group-role.entity';
import { CreateGroupRoleDto } from 'src/dto/groupRole/create-group-role.dto';

@Controller('api/grouproles')
export class GroupRolesController {
    constructor(private readonly groupsRoleService: GroupRolesService) {}

    @Get()
    async find(): Promise<GroupRoleEntity[]> {
        return this.groupsRoleService.getGroupRoleDetail();
    }

    @Post('post')
    async create(@Body() createGroupRoles: CreateGroupRoleDto) {
        try {
            return await this.groupsRoleService.insert(createGroupRoles);
        } catch (Error) {
            return Error;
        }
    }
    @Put('update/:id')
    async update(@Param() id: string, @Body() updateGroupRoleDto: CreateGroupRoleDto) {
        try {
            return await this.groupsRoleService.update(id, updateGroupRoleDto);
        } catch (Error)  {
            return Error;
        }
    }
    @Delete('detele/:id')
    async delete(@Param() id: string) {
        return await this.groupsRoleService.delete(id);
    }
}
