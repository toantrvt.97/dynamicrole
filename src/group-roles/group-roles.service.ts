import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { GroupRoleEntity } from 'src/entity/groupRole/group-role.entity';
import { CreateGroupRoleDto } from 'src/dto/groupRole/create-group-role.dto';
import { GroupEntity } from 'src/entity/group/group.entity';
import { RoleEntity } from 'src/entity/role/role.entity';
import { UserEntity } from 'src/entity/User/user.entity';

@Injectable()
export class GroupRolesService {
    constructor(
    @InjectRepository(GroupRoleEntity)
    private readonly groupRoleRepository: Repository<GroupRoleEntity>,
    @InjectRepository(RoleEntity)
    private readonly roleRepository: Repository<RoleEntity>,
    @InjectRepository(GroupEntity)
    private readonly groupRepository: Repository<GroupEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>) { }

    async insert(groupRoleDetails: CreateGroupRoleDto): Promise<GroupRoleEntity> {
        const groupRoleEntity = this.groupRoleRepository.create();
        const {groupId, roleId, userId, rule} = groupRoleDetails;
        groupRoleEntity.groupId = groupId;
        groupRoleEntity.roleId = roleId;
        groupRoleEntity.userId = userId;
        groupRoleEntity.rule = rule;
        return await this.groupRoleRepository.save(groupRoleEntity);
    }
    async getAllGroupRoles(): Promise<GroupRoleEntity[]> {
        return await this.groupRoleRepository.find();
    }
    async update(id: string, groupRoleDetails: CreateGroupRoleDto ): Promise<GroupRoleEntity> {
        const groups = await this.groupRoleRepository.findOne(id);
        const updated = Object.assign(groups, groupRoleDetails);
        return await this.groupRoleRepository.save(updated);
    }
    async delete(id: string): Promise<DeleteResult> {
        return this.groupRoleRepository.delete(id);
    }
    async getGroupRoleDetail(): Promise<GroupRoleEntity[]> {
        const detail = await this
        .groupRoleRepository
        .createQueryBuilder('groupRole')
        .select('groupRole.id')
        .addSelect('groupRole.rule')
        .addSelect('groupRole.roleName')
        .addSelect('groupRole.groupName')
        .addSelect('groupRole.userName')
        .leftJoin(RoleEntity, 'role', 'role.id = groupRole.roleId')
        .leftJoin(GroupEntity, 'group', 'group.id = groupRole.groupId')
        .leftJoin(UserEntity, 'user', 'user.id = groupRole.userId')
        .getMany();
        return detail;
    }
}
