import { Injectable } from '@nestjs/common';
import { GroupEntity } from 'src/entity/group/group.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { CreateGroupDto } from 'src/dto/group/create-group.dto';
import { WorkspaceEntity } from 'src/entity/workspace/workspace.entity';

@Injectable()
export class GroupsService {
    constructor(
    @InjectRepository(GroupEntity)
    private readonly groupRepository: Repository<GroupEntity>,
    @InjectRepository(WorkspaceEntity)
    private readonly workspaceRepository: Repository<WorkspaceEntity>) { }

    async insert(userDetails: CreateGroupDto): Promise<GroupEntity> {
        const groupEntity = this.groupRepository.create();
        const { name, workspaceId} = userDetails;
        groupEntity.name = name;
        groupEntity.workspaceId = workspaceId;
        return await this.groupRepository.save(groupEntity);
    }
    async getAllGroups(): Promise<GroupEntity[]> {
        return await this.groupRepository.find();
    }
    async update(id: string, groupDetails: CreateGroupDto ): Promise<GroupEntity> {
        const groups = await this.groupRepository.findOne(id);
        const updated = Object.assign(groups, groupDetails);
        return await this.groupRepository.save(updated);
    }
    async delete(id: string): Promise<DeleteResult> {
        return this.groupRepository.delete(id);
    }
    async getGroupDetail(): Promise<GroupEntity[]> {
        const detail = await this
        .groupRepository
        .createQueryBuilder('group')
        .select('group.id')
        .addSelect('group.name')
        .addSelect('group.workspaceName')
        .leftJoin(WorkspaceEntity, 'workspace', 'workspace.id = group.workspaceId')
        .getMany();
        return detail;
    }
}
