import { Module } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupsController } from './groups.controller';
import { GroupEntity } from 'src/entity/group/group.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WorkspaceEntity } from 'src/entity/workspace/workspace.entity';

@Module({
  imports: [TypeOrmModule.forFeature([GroupEntity]), TypeOrmModule.forFeature([WorkspaceEntity])],
  providers: [GroupsService],
  controllers: [GroupsController],
})
export class GroupsModule {}
