import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupEntity } from 'src/entity/group/group.entity';
import { CreateGroupDto } from 'src/dto/group/create-group.dto';

@Controller('api/groups')
export class GroupsController {
    constructor(private readonly groupsService: GroupsService) {}

    @Get()
    async findAll(): Promise<GroupEntity[]> {
        return this.groupsService.getGroupDetail();
    }

    @Post('post')
    async create(@Body() createGroup: CreateGroupDto) {
        try {
            return await this.groupsService.insert(createGroup);
        } catch (Error) {
            return Error;
        }
    }
    @Put('update/:id')
    async update(@Param() id: string, @Body() updateGroupDto: CreateGroupDto) {
        try {
            return await this.groupsService.update(id, updateGroupDto);
        } catch (Error)  {
            return Error;
        }
    }
    @Delete('detele/:id')
    async delete(@Param() id: string) {
        return await this.groupsService.delete(id);
    }
}
