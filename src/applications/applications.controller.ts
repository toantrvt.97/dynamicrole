import { Controller, Get, Post, Body } from '@nestjs/common';
import { ApplicationsService } from './applications.service';
import { ApplicationEntity } from 'src/entity/application/application.entity';
import { CreateApplicationDto } from 'src/dto/application/create-application.dto';

@Controller('api/applications')
export class ApplicationsController {
    constructor(private readonly applicationsService: ApplicationsService) {}

    @Get()
    async findAll(): Promise<ApplicationEntity[]> {
        return this.applicationsService.getAllApp();
    }

    @Post('post')
    async create(@Body() createAppDto: CreateApplicationDto) {
        try {
            return await this.applicationsService.insert(createAppDto);
        } catch (error) {
            return Error;
        }
    }
}
