import { Injectable } from '@nestjs/common';
import { CreateApplicationDto } from 'src/dto/application/create-application.dto';
import { ApplicationEntity } from 'src/entity/application/application.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ApplicationsService {
    constructor(@InjectRepository(ApplicationEntity)
    private readonly appRepository: Repository<ApplicationEntity>) { }

    async insert(appDetails: CreateApplicationDto): Promise<ApplicationEntity> {
        const appEntity = this.appRepository.create();
        const { name } = appDetails;
        appEntity.name = name;
        await this.appRepository.save(appEntity);
        return appEntity;
    }
    async getAllApp(): Promise<ApplicationEntity[]> {
        return await this.appRepository.find();
    }
}
