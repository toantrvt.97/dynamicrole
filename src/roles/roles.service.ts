import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { RoleEntity } from 'src/entity/role/role.entity';
import { CreateRoleDto } from 'src/dto/role/create-role.dto';
import { FunctionEntity } from 'src/entity/function/function.entity';

@Injectable()
export class RolesService {
    constructor(
    @InjectRepository(RoleEntity)
    private readonly roleRepository: Repository<RoleEntity>,
    @InjectRepository(FunctionEntity)
    private readonly functionRepository: Repository<FunctionEntity>,
    ) { }

    async insert(roleDetails: CreateRoleDto): Promise<RoleEntity> {
        const roleEntity = this.roleRepository.create();
        const { name, ftionId} = roleDetails;
        roleEntity.name = name;
        roleEntity.ftionId = ftionId;
        return await this.roleRepository.save(roleEntity);
    }
async getAllRoles(): Promise<RoleEntity[]> {
        return await this.roleRepository.find();
    }
    async update(id: string, roleDetails: CreateRoleDto ): Promise<RoleEntity> {
        const roles = await this.roleRepository.findOne(id);
        const updated = Object.assign(roles, roleDetails);
        return await this.roleRepository.save(updated);
    }
    async delete(id: string): Promise<DeleteResult> {
        return this.roleRepository.delete(id);
    }
    async getRoleDetail(): Promise<RoleEntity[]> {
        const detail = await this
        .roleRepository
        .createQueryBuilder('role')
        .select('role.id')
        .addSelect('role.name')
        .addSelect('role.functionName')
        .leftJoin(FunctionEntity, 'function', 'function.id = role.ftionId')
        .getMany();
        return detail;
    }
}
