import { Module } from '@nestjs/common';
import { RolesController } from './roles.controller';
import { RoleEntity } from 'src/entity/role/role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesService } from './roles.service';
import { FunctionEntity } from 'src/entity/function/function.entity';

@Module({
  imports: [TypeOrmModule.forFeature([RoleEntity]), TypeOrmModule.forFeature([FunctionEntity])],
  controllers: [RolesController],
  providers: [RolesService],
  exports: [RolesService],
})
export class RolesModule {}
