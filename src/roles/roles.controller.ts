import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { RolesService } from './roles.service';
import { RoleEntity } from 'src/entity/role/role.entity';
import { CreateRoleDto } from 'src/dto/role/create-role.dto';

@Controller('api/roles')
export class RolesController {
    constructor(private readonly rolesService: RolesService) {}

    @Get()
    async findAll(): Promise<RoleEntity[]> {
        return this.rolesService.getRoleDetail();
    }

    @Post('post')
    async create(@Body() createRole: CreateRoleDto) {
        try {
            return await this.rolesService.insert(createRole);
        } catch (Error) {
            return Error;
        }
    }
    @Put('update/:id')
    async update(@Param() id: string, @Body() updateRoleDto: CreateRoleDto) {
        try {
            return await this.rolesService.update(id, updateRoleDto);
        } catch (Error)  {
            return Error;
        }
    }
    @Delete('detele/:id')
    async delete(@Param() id: string) {
        return await this.rolesService.delete(id);
    }
}
