import { ApiProperty } from '@nestjs/swagger';

export class CreateGroupDto {
    @ApiProperty()
    readonly name: string;
    @ApiProperty()
    readonly workspaceId: string;
}
