import { ApiProperty } from '@nestjs/swagger';

export class CreateFunctionDto {
    @ApiProperty()
    readonly name: string;
}
