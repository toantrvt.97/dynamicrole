import { CreateFunctionDto } from './create-function.dto';

describe('CreateFunctionDto', () => {
  it('should be defined', () => {
    expect(new CreateFunctionDto()).toBeDefined();
  });
});
