import { ApiProperty } from '@nestjs/swagger';

export class CreateApplicationDto {
    @ApiProperty()
    name: string;
}
