import { CreateApplicationDto } from './create-application.dto';

describe('CreateApplicationDto', () => {
  it('should be defined', () => {
    expect(new CreateApplicationDto()).toBeDefined();
  });
});
