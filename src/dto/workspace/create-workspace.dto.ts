import { ApiProperty } from '@nestjs/swagger';

export class CreateWorkspaceDto {
    @ApiProperty()
    readonly name: string;
    @ApiProperty()
    readonly appId: string;

}
