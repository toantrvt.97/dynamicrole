import { CreateWorkspaceDto } from './create-workspace.dto';

describe('CreateWorkspaceDto', () => {
  it('should be defined', () => {
    expect(new CreateWorkspaceDto()).toBeDefined();
  });
});
