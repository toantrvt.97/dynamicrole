import { ApiProperty } from '@nestjs/swagger';

export class CreateGroupRoleDto {
    @ApiProperty()
    readonly groupId: string;
    @ApiProperty()
    readonly roleId: string;
    @ApiProperty()
    readonly userId: string;
    @ApiProperty()
    readonly rule: boolean;

}
