import { CreateGroupRoleDto } from './create-group-role.dto';

describe('CreateGroupRoleDto', () => {
  it('should be defined', () => {
    expect(new CreateGroupRoleDto()).toBeDefined();
  });
});
