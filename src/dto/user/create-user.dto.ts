import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
    @ApiProperty()
    readonly name: string;
    @ApiProperty()
    readonly roleId: string;
    @ApiProperty()
    readonly groupId: string;

}
