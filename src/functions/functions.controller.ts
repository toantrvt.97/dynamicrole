import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { FunctionsService } from './functions.service';
import { FunctionEntity } from 'src/entity/function/function.entity';
import { CreateFunctionDto } from 'src/dto/function/create-function.dto';

@Controller('api/functions')
export class FunctionsController {
    constructor(private readonly functionsService: FunctionsService) {}

    @Get()
    async findAll(): Promise<FunctionEntity[]> {
        return this.functionsService.getAllFunctions();
    }

    @Post('post')
    async create(@Body() createFunctionDto: CreateFunctionDto) {
        try {
            return await this.functionsService.insert(createFunctionDto);
        } catch (Error) {
            return Error;
        }
    }
    @Put('update/:id')
    async update(@Param() id: string, @Body() updateFunctionDto: CreateFunctionDto) {
        try {
            return await this.functionsService.update(id, updateFunctionDto);
        } catch (Error)  {
            return Error;
        }
    }
    @Delete('detele/:id')
    async delete(@Param() params) {
        return await this.functionsService.delete(params.id);
    }
}
