import { Module } from '@nestjs/common';
import { FunctionsController } from './functions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FunctionEntity } from 'src/entity/function/function.entity';
import { FunctionsService } from './functions.service';

@Module({
  imports: [TypeOrmModule.forFeature([FunctionEntity])],
  controllers: [FunctionsController],
  providers: [FunctionsService],
  exports: [FunctionsService],
})
export class FunctionsModule {}
