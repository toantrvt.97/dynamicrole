import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FunctionEntity } from 'src/entity/function/function.entity';
import { Repository, DeleteResult } from 'typeorm';
import { CreateFunctionDto } from 'src/dto/function/create-function.dto';

@Injectable()
export class FunctionsService {
    constructor(@InjectRepository(FunctionEntity)
    private readonly functionRepository: Repository<FunctionEntity>) { }

    async insert(funtionDetails: CreateFunctionDto): Promise<FunctionEntity> {
        const functionEntity = this.functionRepository.create();
        const { name } = funtionDetails;
        functionEntity.name = name;
        return await this.functionRepository.save(functionEntity);
    }
    async getAllFunctions(): Promise<FunctionEntity[]> {
        return await this.functionRepository.find();
    }
    async update(id: string, funtionDetails: CreateFunctionDto ): Promise<FunctionEntity> {
        const functions = await this.functionRepository.findOne(id);
        const updated = Object.assign(functions, funtionDetails);
        return await this.functionRepository.save(updated);
    }
    async delete(id: string): Promise<DeleteResult> {
        return this.functionRepository.delete(id);
    }
}
