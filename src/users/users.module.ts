import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/entity/User/user.entity';
import { UsersService } from './users.service';
import { RoleEntity } from 'src/entity/role/role.entity';
import { GroupEntity } from 'src/entity/group/group.entity';

@Module({
  imports:
  [TypeOrmModule.forFeature([UserEntity]),
  TypeOrmModule.forFeature([RoleEntity]),
  TypeOrmModule.forFeature([GroupEntity])],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
