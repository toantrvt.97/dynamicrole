import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { UserEntity } from 'src/entity/User/user.entity';
import { CreateUserDto } from 'src/dto/user/create-user.dto';
import { GroupEntity } from 'src/entity/group/group.entity';
import { RoleEntity } from 'src/entity/role/role.entity';

@Injectable()
export class UsersService {
    constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(GroupEntity)
    private readonly groupRepository: Repository<GroupEntity>,
    @InjectRepository(RoleEntity)
    private readonly roleRepository: Repository<RoleEntity>) { }

    async insert(userDetails: CreateUserDto): Promise<UserEntity> {
        const userEntity = this.userRepository.create();
        const { name, roleId, groupId } = userDetails;
        userEntity.name = name;
        userEntity.groupId = groupId;
        userEntity.roleId = roleId;
        userEntity.groupName = await (await this.groupRepository.findOne(groupId)).name;
        userEntity.roleName = await (await this.roleRepository.findOne(roleId)).name;
        return await this.userRepository.save(userEntity);
    }
    async getAllUsers(): Promise<UserEntity[]> {
        return await this.userRepository.find();
    }
    async update(id: string, funtionDetails: CreateUserDto ): Promise<UserEntity> {
        const functions = await this.userRepository.findOne(id);
        const updated = Object.assign(functions, funtionDetails);
        return await this.userRepository.save(updated);
    }
    async delete(id: string): Promise<DeleteResult> {
        return this.userRepository.delete(id);
    }
    async getUserDetail(): Promise<UserEntity[]> {
        const detail = await this
        .userRepository
        .createQueryBuilder('user')
        .select('user.id')
        .addSelect('user.name')
        .addSelect('user.groupName')
        .addSelect('user.roleName')
        .leftJoin(GroupEntity, 'group', 'group.id = user.groupId')
        .leftJoin(RoleEntity, 'role', 'role.id = user.roleId')
        .getMany();
        return detail;
    }
}
