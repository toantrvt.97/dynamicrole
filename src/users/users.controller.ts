import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserEntity } from 'src/entity/User/user.entity';
import { CreateUserDto } from 'src/dto/user/create-user.dto';

@Controller('api/users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Get()
    async findAll(): Promise<UserEntity[]> {
        return this.usersService.getUserDetail();
    }

    @Post('post')
    async create(@Body() createUser: CreateUserDto) {
        try {
            return await this.usersService.insert(createUser);
        } catch (Error) {
            return Error;
        }
    }
    @Put('update/:id')
    async update(@Param() id: string, @Body() updateUserDto: CreateUserDto) {
        try {
            return await this.usersService.update(id, updateUserDto);
        } catch (Error)  {
            return Error;
        }
    }
    @Delete('detele/:id')
    async delete(@Param() id: string) {
        return await this.usersService.delete(id);
    }
}
